package com.surecloud.javatechnicalinterview.exception;

import com.surecloud.javatechnicalinterview.model.ApiErrorResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.servlet.http.HttpServletRequest;

@ControllerAdvice
public class SureCloudExceptionHandler {

  private static final Logger LOG = LoggerFactory.getLogger(SureCloudExceptionHandler.class);

  @ExceptionHandler(ResultNotFoundException.class)
  public ResponseEntity<ApiErrorResponse> handle(HttpServletRequest req, ResultNotFoundException e) {
    LOG.error("Request generated ResultNotFoundException, dealing with it", e);
    final ApiErrorResponse errorResponse = new ApiErrorResponse(e.getErrorMessage());
    return new ResponseEntity<>(errorResponse, HttpStatus.NOT_FOUND);
  }

  @ExceptionHandler(Exception.class)
  public ResponseEntity<ApiErrorResponse> handle(HttpServletRequest req, Exception e) {
    LOG.error("Request generated Exception, dealing with it", e);
    final ApiErrorResponse errorResponse = new ApiErrorResponse(e.getMessage());
    return new ResponseEntity<>(errorResponse, HttpStatus.BAD_REQUEST);
  }

}
