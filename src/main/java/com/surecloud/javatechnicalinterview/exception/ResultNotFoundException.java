package com.surecloud.javatechnicalinterview.exception;

public class ResultNotFoundException extends Exception {

  public ResultNotFoundException(String errorMessage) {
    super();
    this.errorMessage = errorMessage;
  }

  private final String errorMessage;

  public String getErrorMessage() {
    return errorMessage;
  }

}
