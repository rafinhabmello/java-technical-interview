package com.surecloud.javatechnicalinterview.exception;

public class InvalidRequestException extends Exception {

  public InvalidRequestException(String errorMessage) {
    super();
    this.errorMessage = errorMessage;
  }

  private final String errorMessage;

  public String getErrorMessage() {
    return errorMessage;
  }

}
