package com.surecloud.javatechnicalinterview.service;

import com.surecloud.javatechnicalinterview.exception.ResultNotFoundException;
import com.surecloud.javatechnicalinterview.model.Result;
import com.surecloud.javatechnicalinterview.repository.ResultRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class ResultService {

  @Autowired
  private ResultRepository resultRepository;

  public List<Result> getResults() {
    return resultRepository.findAll();
  }

  public Result getResultById(UUID id) throws ResultNotFoundException {
    Optional<Result> result = resultRepository.findById(id);
    if (!result.isPresent()) {
      throw new ResultNotFoundException(String.format("Result with id %s was not found", id));
    }
    return result.get();
  }

  public Result addResult(Result result) {
    return resultRepository.save(result);
  }

}
