package com.surecloud.javatechnicalinterview.controller;

import com.surecloud.javatechnicalinterview.exception.ResultNotFoundException;
import com.surecloud.javatechnicalinterview.model.Result;
import com.surecloud.javatechnicalinterview.service.ResultService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping(value = "/result")
public class ResultController {

  @Autowired
  private ResultService resultService;

  @RequestMapping(
      produces = {MediaType.APPLICATION_JSON_VALUE},
      method = RequestMethod.GET
  )
  public ResponseEntity<List<Result>> getResults() {
    List<Result> results = resultService.getResults();
    return new ResponseEntity<>(results, HttpStatus.OK);
  }

  @RequestMapping(
      value = "/{resultId}",
      produces = {MediaType.APPLICATION_JSON_VALUE},
      method = RequestMethod.GET
  )
  public ResponseEntity<Result> getResultById(@PathVariable final UUID resultId) throws ResultNotFoundException {
    Result result = resultService.getResultById(resultId);
    return new ResponseEntity<>(result, HttpStatus.OK);
  }

  @RequestMapping(
      consumes = {MediaType.APPLICATION_JSON_VALUE},
      produces = {MediaType.APPLICATION_JSON_VALUE},
      method = RequestMethod.POST
  )
  public ResponseEntity<Result> addResult(@RequestBody Result newResult) {
    Result result = resultService.addResult(newResult);
    return new ResponseEntity<>(result, HttpStatus.CREATED);
  }

}
