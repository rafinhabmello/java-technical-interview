package com.surecloud.javatechnicalinterview.model;

public class ApiErrorResponse {

  public ApiErrorResponse(String errorMessage) {
    this.errorMessage = errorMessage;
  }

  private String errorMessage;

  public String getErrorMessage() {
    return errorMessage;
  }

  public void setErrorMessage(String errorMessage) {
    this.errorMessage = errorMessage;
  }

}
