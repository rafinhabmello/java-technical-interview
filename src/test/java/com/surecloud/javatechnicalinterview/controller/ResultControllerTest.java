package com.surecloud.javatechnicalinterview.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.surecloud.javatechnicalinterview.exception.ResultNotFoundException;
import com.surecloud.javatechnicalinterview.model.Result;
import com.surecloud.javatechnicalinterview.service.ResultService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

@AutoConfigureMockMvc
@SpringBootTest
public class ResultControllerTest {

  private static final UUID ID = UUID.fromString("a8078fd7-0f46-4d11-868b-44e3d0cf8731");
  private static final String NAME = "Test Name";
  private static final int SCORE = 77;
  private static final Date DATE = new Date();

  @MockBean
  private ResultService resultService;

  @Autowired
  private MockMvc mvc;

  @Autowired
  private ObjectMapper objectMapper;

  @Test
  public void getResults_shouldReturnExistingResults() throws Exception {
    List<Result> resultList = new ArrayList<>();
    resultList.add(createResultObject());
    when(resultService.getResults()).thenReturn(resultList);

    mvc.perform(get("/result"))
        .andExpect(jsonPath("$.[0].name").value(NAME))
        .andExpect(jsonPath("$.[0].score").value(SCORE))
        .andExpect(MockMvcResultMatchers.status().isOk());

  }

  @Test
  public void getResultById_shouldReturnExistentResult() throws Exception {
    when(resultService.getResultById(ID)).thenReturn(createResultObject());

    mvc.perform(get("/result/" + ID))
        .andExpect(jsonPath("$.name").value(NAME))
        .andExpect(jsonPath("$.score").value(SCORE))
        .andExpect(MockMvcResultMatchers.status().isOk());

  }

  @Test
  public void getResultById_shouldReturnNotFound_ifResultDoesNotExist() throws Exception {
    String errorMessage = "not found";
    when(resultService.getResultById(ID)).thenThrow(new ResultNotFoundException(errorMessage));

    mvc.perform(get("/result/" + ID))
        .andExpect(jsonPath("$.errorMessage").value(errorMessage))
        .andExpect(MockMvcResultMatchers.status().isNotFound());
  }

  @Test
  public void postResult_shouldSaveAndReturnNewResult() throws Exception {
    Result result = createResultObject();
    when(resultService.addResult(any())).thenReturn(result);

    mvc.perform(post("/result")
            .content(objectMapper.writeValueAsString(result))
            .contentType(MediaType.APPLICATION_JSON))
        .andExpect(jsonPath("$.name").value(NAME))
        .andExpect(jsonPath("$.score").value(SCORE))
        .andExpect(MockMvcResultMatchers.status().isCreated());

  }

  private Result createResultObject() {
    Result result = new Result();
    result.setId(ID);
    result.setName(NAME);
    result.setScore(SCORE);
    result.setDateTaken(DATE);
    return result;
  }

}
