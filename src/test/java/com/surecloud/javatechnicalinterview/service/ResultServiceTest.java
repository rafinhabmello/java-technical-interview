package com.surecloud.javatechnicalinterview.service;

import com.surecloud.javatechnicalinterview.exception.ResultNotFoundException;
import com.surecloud.javatechnicalinterview.model.Result;
import com.surecloud.javatechnicalinterview.repository.ResultRepository;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

@SpringBootTest
public class ResultServiceTest {

  private static final UUID ID = UUID.fromString("a8078fd7-0f46-4d11-868b-44e3d0cf8731");
  private static final String NAME = "Test Name";
  private static final int SCORE = 77;
  private static final Date DATE = new Date();

  @InjectMocks
  private ResultService resultService;

  @Mock
  private ResultRepository resultRepository;

  @Test
  public void getResults_shouldReturnExistentResults() {
    List<Result> resultList = new ArrayList<>();
    resultList.add(createResultObject());

    when(resultRepository.findAll()).thenReturn(resultList);

    List<Result> allResults = resultService.getResults();
    assertFalse(allResults.isEmpty());
    assertEquals(1, allResults.size());

    Result firstResult = allResults.get(0);
    assertEquals(ID, firstResult.getId());
    assertEquals(NAME, firstResult.getName());
    assertEquals(SCORE, firstResult.getScore());
    assertEquals(DATE, firstResult.getDateTaken());
  }

  @Test
  public void getResultById_shouldReturnExistentResult() throws ResultNotFoundException {
    when(resultRepository.findById(eq(ID))).thenReturn(Optional.of(createResultObject()));

    Result result = resultService.getResultById(ID);
    assertNotNull(result);
    assertEquals(ID, result.getId());
    assertEquals(NAME, result.getName());
    assertEquals(SCORE, result.getScore());
    assertEquals(DATE, result.getDateTaken());
  }

  @Test
  public void getResultById_shouldThrow_ifResultDoesNotExist() {
    when(resultRepository.findById(eq(ID))).thenReturn(Optional.empty());

    assertThatThrownBy(() -> resultService.getResultById(ID))
        .isInstanceOf(ResultNotFoundException.class);
  }

  @Test
  public void addResult_shouldSaveAndReturnNewResult() {
    Result resultObject = createResultObject();
    when(resultRepository.save(any())).thenReturn(resultObject);

    Result newResult = resultService.addResult(resultObject);
    assertNotNull(newResult);
    assertEquals(ID, newResult.getId());
    assertEquals(NAME, newResult.getName());
    assertEquals(SCORE, newResult.getScore());
    assertEquals(DATE, newResult.getDateTaken());
  }

  private Result createResultObject() {
    Result result = new Result();
    result.setId(ID);
    result.setName(NAME);
    result.setScore(SCORE);
    result.setDateTaken(DATE);
    return result;
  }

}
